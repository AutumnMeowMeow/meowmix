;;;; -*- Mode: Lisp; Syntax: ANSI-Common-Lisp; Base: 10 -*-
;;;;
;;;; Written 2022 by Autumn Lamonte ⚧ Trans Liberation Now
;;;;
;;;; This is free and unencumbered software released into the public domain.
;;;;
;;;; Anyone is free to copy, modify, publish, use, compile, sell, or
;;;; distribute this software, either in source code form or as a compiled
;;;; binary, for any purpose, commercial or non-commercial, and by any means.
;;;;
;;;; In jurisdictions that recognize copyright laws, the author or authors of
;;;; this software dedicate any and all copyright interest in the software to
;;;; the public domain. We make this dedication for the benefit of the public
;;;; at large and to the detriment of our heirs and successors. We intend
;;;; this dedication to be an overt act of relinquishment in perpetuity of
;;;; all present and future rights to this software under copyright law.
;;;;
;;;; THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
;;;; OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
;;;; MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
;;;; NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM,
;;;; DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
;;;; OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR
;;;; THE USE OR OTHER DEALINGS IN THE SOFTWARE.
;;;;
;;;; For more information, please refer to <http://unlicense.org/>
;;;;
;;;; --------------------------------------------------------------------------

(in-package #:mm-tui)

;;; Constants -----------------------------------------------------------------

;;; Global objects ------------------------------------------------------------

;;; Implementation ------------------------------------------------------------

(defun screen-resized (width height)
  (log-info "SCREEN RESIZED: ~d x ~d" width height))

(defun keyboard-input (ch shift ctrl alt)
  (log-info "KEYBOARD: ~c shift:~a ctrl:~a alt:~a" ch shift ctrl alt))

(defun mouse-input (type x y button1 button2 button3 wheel-up wheel-down
		      shift ctrl alt)
  (log-info "MOUSE: ~a (~d,~d) 1:~a 2:~a 3:~a up:~a down:~a shift:~a ctrl:~a alt:~a"
	    type x y button1 button2 button3 wheel-up wheel-down
	    shift ctrl alt))

(defun mm-demo ()
  ;; Write to a log file.
  (let ((log-file (open "mm-tui.log"
			:direction :output
			:if-exists :append
			:if-does-not-exist :create)))

    (set-log-output log-file)
    (log-info "MeowMix TUI demo starting...")
    (stty-raw)

    ;; Bind the callback functions and run the main loop.
    (let ((*screen-resize-fn* #'screen-resized)
	  (*keyboard-input-fn* #'keyboard-input)
	  (*mouse-input-fn* #'mouse-input))
      (main-loop))

    (stty-cooked)
    (log-info "MeowMix TUI demo exit.")
    (close log-file))

  ;; All done.
  t)
