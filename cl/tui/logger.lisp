;;;; -*- Mode: Lisp; Syntax: ANSI-Common-Lisp; Base: 10 -*-
;;;;
;;;; Written 2022 by Autumn Lamonte ⚧ Trans Liberation Now
;;;;
;;;; This is free and unencumbered software released into the public domain.
;;;;
;;;; Anyone is free to copy, modify, publish, use, compile, sell, or
;;;; distribute this software, either in source code form or as a compiled
;;;; binary, for any purpose, commercial or non-commercial, and by any means.
;;;;
;;;; In jurisdictions that recognize copyright laws, the author or authors of
;;;; this software dedicate any and all copyright interest in the software to
;;;; the public domain. We make this dedication for the benefit of the public
;;;; at large and to the detriment of our heirs and successors. We intend
;;;; this dedication to be an overt act of relinquishment in perpetuity of
;;;; all present and future rights to this software under copyright law.
;;;;
;;;; THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
;;;; OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
;;;; MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
;;;; NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM,
;;;; DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
;;;; OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR
;;;; THE USE OR OTHER DEALINGS IN THE SOFTWARE.
;;;;
;;;; For more information, please refer to <http://unlicense.org/>
;;;;
;;;; --------------------------------------------------------------------------

(in-package #:mm-tui)

;;;; Simple logging functions
;;;;
;;;; Every module/subprogram can use the logger to report messages to the
;;;; user, developer, etc.  Generally, one simply does this:
;;;;
;;;; (make-instance logger :spec "modulename")
;;;;
;;;; or
;;;;
;;;; (make-instance logger :spec "modulename" :level
;;;;                +log-level-info+)
;;;;
;;;; Then use the logger by invoking the "log-X" function and passing
;;;; arguments similar to format:
;;;;
;;;; (log-info logger "Message! ~a" arg1)
;;;;
;;;; One can also omit the logger argument and output will go to a common
;;;; logger.
;;;;
;;;; There are a number of logging levels, saved in *log-levels*:
;;;;
;;;; ERROR      Error
;;;; WARNING    Warning
;;;; INFO       Informational (regular logging)
;;;; DEBUG1     Debug (slight - important parameters)
;;;; DEBUG2     Debug (moderate - step through)

;;; Constants -----------------------------------------------------------------

(defconstant +log-level-error+ 1)
(defconstant +log-level-warning+ 2)
(defconstant +log-level-info+ 3)
(defconstant +log-level-debug1+ 4)
(defconstant +log-level-debug2+ 5)

;;; Global objects ------------------------------------------------------------

(defclass logger ()
  ((spec :initarg :spec)
   (level :initarg :level :initform +log-level-info+ :accessor log-level)
   (output :initarg :output :initform *standard-output* :accessor log-output)))

(defvar *global-logger* (make-instance 'logger :spec ""
				       :level +log-level-debug1+))

;;; Implementation ------------------------------------------------------------

(defun set-log-level (level)
  "Sets global logging level"
  (declare (type integer level))
  (setf (log-level *global-logger*) level))

(defun set-log-output (output)
  "Sets global logging output"
  (setf (log-output *global-logger*) output))

(defgeneric log-msg (object tagchar msg))

;; Cache the timestamp -- this improves performance quite a bit when
;; there are a lot of debugging messages.
(let ((cached-timestamp "")
      (cached-time 0))
  (declare (type string cached-timestamp))
  (declare (type integer cached-time))

  (defun log-make-timestamp (currenttime)
    "Make a standard timestamp string"
    (multiple-value-bind
     (second minute hour day-of-month month year
	     #| day-of-week dst-p tz |#)
     (decode-universal-time currenttime 0)
     (format nil "~4,'0D-~2,'0D-~2,'0D ~2,'0D:~2,'0D:~2,'0D"
	     year month day-of-month hour minute second)))

  (defmethod log-msg ((logger logger) tagchar msg)
    "Logging method for logging class, which includes PID and spec."
    (declare (type string tagchar))

    (let ((currenttime (get-universal-time))
	  (threadid
	   #+:CMU (unix:unix-getpid)
	   #+:SBCL (sb-unix:unix-getpid)))

      (declare (type integer currenttime))
      (declare (type (signed-byte 32) threadid))

      (if (not (= currenttime cached-time))
	  (progn
	    (setf cached-time currenttime)
	    (setf cached-timestamp (log-make-timestamp currenttime))))

      (if (> (length (slot-value logger 'spec)) 0)
	  (format (log-output logger) "[~a] ~6,'0D ~a "
		  cached-timestamp threadid
		  (slot-value logger 'spec))
	(format (log-output logger) "[~a] ~6,'0D "
		cached-timestamp threadid))
      (format (log-output logger) "~a " tagchar)
      (apply #'format (log-output logger) msg)
      (format (log-output logger) "~%")))

  (defmethod log-msg ((string string) tagchar msg)
    "General logging method with only timestamp and message."
    (declare (type string tagchar))
    (log-msg *global-logger* tagchar (append (list string) msg))))

;; log-X macros
(defmacro log-error (string &rest msg)
  `(if (typep ,string 'mm-tui::logger)
       (if (<= +log-level-error+ (log-level ,string))
	   (apply #'log-msg (list ,string "E" (list ,@msg))))
     (if (<= +log-level-error+ (log-level *global-logger*))
	 (apply #'log-msg (list ,string "E" (list ,@msg))))))
(defmacro log-warning (string &rest msg)
  `(if (typep ,string 'mm-tui::logger)
       (if (<= +log-level-warning+ (log-level ,string))
	   (apply #'log-msg (list ,string "W" (list ,@msg))))
     (if (<= +log-level-warning+ (log-level *global-logger*))
	 (apply #'log-msg (list ,string "W" (list ,@msg))))))
(defmacro log-info (string &rest msg)
  `(if (typep ,string 'mm-tui::logger)
       (if (<= +log-level-info+ (log-level ,string))
	   (apply #'log-msg (list ,string "I" (list ,@msg))))
     (if (<= +log-level-info+ (log-level *global-logger*))
	 (apply #'log-msg (list ,string "I" (list ,@msg))))))
(defmacro log-debug1 (string &rest msg)
  `(if (typep ,string 'mm-tui::logger)
       (if (<= +log-level-debug1+ (log-level ,string))
	   (apply #'log-msg (list ,string "D1" (list ,@msg))))
     (if (<= +log-level-debug1+ (log-level *global-logger*))
	 (apply #'log-msg (list ,string "D1" (list ,@msg))))))
(defmacro log-debug2 (string &rest msg)
  `(if (typep ,string 'mm-tui::logger)
       (if (<= +log-level-debug2+ (log-level ,string))
	   (apply #'log-msg (list ,string "D2" (list ,@msg))))
     (if (<= +log-level-debug2+ (log-level *global-logger*))
	 (apply #'log-msg (list ,string "D2" (list ,@msg))))))

;; Special function for logging Lisp errors
(defun log-caught-error (logger &rest error-arg)
  (let ((lisp-error (car error-arg)))
    (if (typep logger 'logger)
	(if (typep lisp-error 'simple-condition)
	    (log-error
	     logger
	     (apply #'format
		    (append
		     (list nil (cl:simple-condition-format-control lisp-error))
		     (cl:simple-condition-format-arguments lisp-error))))
	  (log-error logger "~a" lisp-error))

      (if (typep lisp-error 'simple-condition)
	  (log-error
	   (apply #'format
		  (append (list nil (cl:simple-condition-format-control logger))
			  (cl:simple-condition-format-arguments logger))))
	(log-error "~a" logger)))))
