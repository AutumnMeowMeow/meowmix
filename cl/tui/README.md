MeowMix TUI
===========

This will eventually be a text user interface for Xterm-like terminals
with a simple functional programming API.

For history of other projects I have done here:
https://jexer.sourceforge.io/evolution.html



TODO
----

Keyboard input, main output loop
   read-char-no-hang can read from stdin without hanging allegedly

Hello world as fullscreen app: screen setup and teardown via stty

Macro-protected debug logging to stderr



Wishlist
--------

YAML or JSON for defining UI elements



Work Log
--------

August 12, 2022

Been thinking about the design.  I think I will start with structures
(defstruct) rather than objects.  Also, what goes on screen will be a
...  fragment?  No, "speck".  A speck will contain attributes (color,
bold/underline/etc.) and glyph(s), and have functions to determine
number of cells to display, number of codepoints in it, insert,
delete, split, concatenate: basically it's a string with attributes.

...

We've got stty raw, stty cooked, and stty size presumably working.
Thanks to UIOP, this is much easier than it used to be.  I'm beginning
to recall mapcar and functional programming again.

August 11, 2022

I'm back in CommonLisp, first time since September 2006.  18 years?!

I want to write a nearly-fully-functional-programming simple TUI, but
with excellent look and behavior on Xterm (basically most of Jexer's
ECMA48Terminal pre-sixel).  Load it up, display basic text elements,
handle callbacks (lambdas), and be pluggable into larger programs
without dictating too much about said program's structure.  Do better
solving the "composing many widgets on screen" problem, and maybe be
able to do fancier things like immediate mode GUI.

I don't want to do the "beat Turbo Vision" thing.  Too many widgets no
one needs, too much stuff just to do it.  Instead I'm hoping with
judicious use of macros and functional programming, the core can work
and be smaller than 10 KLOC.  So only Xterm/ECMA48 backend, no use of
(n)curses, and no C FFI.  Basically tui-rs, termbox, tcell, etc.  But
much prettier, and capable of sustaining several different UI models.

Things to do differently than Jexer et al:

* The grid coordinates are just the spot were we output.  Length of
  text strings will be computed better, and trimmed by whole character
  to fit within widget boundaries.  Let's plan for grapheme clusters
  from the get-go rather than fixed-cell "one cell is one character"
  stuff.

* Make far more use of tables and state machine, and less hardcoded
  paths and switch statements.  Use the real power of CL macros to cut
  out all the boilerplate.

* IFF I bother implementing a terminal widget, then use a completely
  fresh codebase rather than the same Qodem-derived emulator I've used
  like four times already.

* No Goddess objects.  Be composable small things, and not the massive
  TApplication / TWidget / TWindow mess.

* No singletons, and be designed for multihead from the get-go.  Any
  screen can be written at any time to any backend.  That may mean
  that different screens can have different dimensions, and we clip on
  output.

* Now that I know what is reliable for sixel: horizontal access is
  more important than vertical or (x, y) random access.  By which I
  mean almost all widgets are just collections of lines, not real 2D
  arrays.  I want to design this to be capable of using
  proportional-width fonts.

* I want to avoid CLOS, and OO in general.  Compose out of little
  things instead of a complex inheritance map.

* I'd like a different rendering model than painter's algorithm.  But
  still not sure how to go about it.

I will say: it's nice to have a blank page again.
