;;;; -*- Mode: Lisp; Syntax: ANSI-Common-Lisp; Base: 10 -*-
;;;;
;;;; Written 2022 by Autumn Lamonte ⚧ Trans Liberation Now
;;;;
;;;; This is free and unencumbered software released into the public domain.
;;;;
;;;; Anyone is free to copy, modify, publish, use, compile, sell, or
;;;; distribute this software, either in source code form or as a compiled
;;;; binary, for any purpose, commercial or non-commercial, and by any means.
;;;;
;;;; In jurisdictions that recognize copyright laws, the author or authors of
;;;; this software dedicate any and all copyright interest in the software to
;;;; the public domain. We make this dedication for the benefit of the public
;;;; at large and to the detriment of our heirs and successors. We intend
;;;; this dedication to be an overt act of relinquishment in perpetuity of
;;;; all present and future rights to this software under copyright law.
;;;;
;;;; THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
;;;; OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
;;;; MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
;;;; NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM,
;;;; DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
;;;; OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR
;;;; THE USE OR OTHER DEALINGS IN THE SOFTWARE.
;;;;
;;;; For more information, please refer to <http://unlicense.org/>
;;;;
;;;; --------------------------------------------------------------------------

(in-package #:mm-tui)

;;; Constants -----------------------------------------------------------------

;;; Global objects ------------------------------------------------------------

(defvar *keyboard-input-fn* nil)

(defvar *mouse-input-fn* nil)

(defvar *screen-resize-fn* nil)

;;; Implementation ------------------------------------------------------------

(defun stty-raw ()
  "Set the tty of standard input/output to raw bytes mode."
  (log-debug1 "stty-raw")
  (run-program (list "/bin/sh" "-c" "stty -ignbrk -brkint -parmrk -istrip -inlcr -igncr -icrnl -ixon -opost -echo -echonl -icanon -isig -iexten -parenb cs8 min 1 < /dev/tty")
               :input :interactive
               :output :interactive
               :ignore-error-status t))

(defun stty-cooked ()
  "Set the tty of standard input/output to line-oriented mode."
  (log-debug1 "stty-cooked")
  (run-program (list "/bin/sh" "-c" "stty sane cooked < /dev/tty")
               :input :interactive
               :output :interactive
               :ignore-error-status t))

(defun stty-size ()
  "Get the rows and columns of the tty of standard input/output."
  (log-debug2 "stty-size")
  (mapcar #'parse-integer
          (uiop:split-string
           (run-program (list "/bin/sh" "-c" "stty size < /dev/tty")
                        :input :interactive
                        :output '(:string :stripped t)
                        :ignore-error-status t))))

(let ((screen-width 0)
      (screen-height 0)
      (last-screen-time 0)
      (exit-loop nil))

  (defun exit-main-loop ()
    "Set the exit flag, causing the main loop to exit."
    (setf exit-loop t))

  (defun main-loop ()
    "Get keyboard and mouse input and call functions to process them. Also
check screen dimensions and call a function if it changes."

    (loop while (not exit-loop) do
          (progn

            ;; Check for screen size change every 3 seconds.
            (let ((currenttime (get-universal-time)))
              (if (not (<= currenttime (+ last-screen-time 3)))
                  (progn
                    (setf cached-time currenttime)
                    (let* ((new-dims (stty-size))
                           (new-height (car new-dims))
                           (new-width (cadr new-dims)))
                      (if (or (not (= new-width screen-width))
                              (not (= new-height screen-height)))
                          (progn
                            (log-debug1 "New screen dimensions: ~D rows ~D cols"
                                        new-height new-width)
                            (setf screen-width new-width)
                            (setf screen-height new-height)

                            ;; Screen size changed, notify caller.
                            (if *screen-resize-fn*
                                (apply *screen-resize-fn*
                                       (list screen-width screen-height)))))))))

            ;; DEBUG - just do it once
            (setf exit-loop t))

     ;; TODO

    ))

  )
