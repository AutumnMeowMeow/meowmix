/*
 * Written 2022 by Autumn Lamonte ⚧ Trans Liberation Now
 *
 * This is free and unencumbered software released into the public domain.
 *
 * Anyone is free to copy, modify, publish, use, compile, sell, or distribute
 * this software, either in source code form or as a compiled binary, for any
 * purpose, commercial or non-commercial, and by any means.
 *
 * In jurisdictions that recognize copyright laws, the author or authors of
 * this software dedicate any and all copyright interest in the software to
 * the public domain. We make this dedication for the benefit of the public
 * at large and to the detriment of our heirs and successors. We intend this
 * dedication to be an overt act of relinquishment in perpetuity of all
 * present and future rights to this software under copyright law.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
 * THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
 * IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
 * CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *
 * For more information, please refer to <http://unlicense.org/>
 *
 * ----------------------------------------------------------------------------
 *
 * Realtime monitor program.
 *
 * This program reads a FIFO specified by the -i argument and writes in
 * non-blocking mode to a FIFO specified by the -o argument.  'cat'ing the
 * output FIFO will provide a realtime monitor screen.
 *
 * By default, it forks and runs in the background.  Recompile with DEBUG to
 * keep in foreground and emit diagnostic messages.
 *
 * Invocation:  monitor { -i input_fifo } { -o output_fifo } { -p pid_file }
 *
 * Returns:      0 OK
 *               3 Unable to set a signal handler
 *              20 Out of memory
 *              21 Error accessing a file
 *              70 Operating system error
 *              99 Unknown arguments
 *
 */

#include <errno.h>
#include <stdlib.h>
#include <signal.h>
#include <string.h>
#include <unistd.h>
#include <fcntl.h>
#include "utils.h"
#include "logging.h"

/* #define DEBUG        1 */
#undef DEBUG

/* PID filename */
static char * pid_filename = NULL;

/* Input FIFO */
static char * input_fifo = NULL;

/* Output FIFO */
static char * output_fifo = NULL;

/*
 * Show the usage options:
 *      -h -?            Usage screen
 *      -i filename      Input FIFO
 *      -o filename      Output FIFO
 *      -p filename      Output PID to filename
 *
 * @param  progname     Program name
 * @return void
 */
static void show_usage(const char * progname) {
    fprintf(stderr, "USAGE: %s { -h | -? | { -i input_fifo } { -o output_fifo }\n",
        progname);
    fprintf(stderr, "       -h -?            Display usage screen\n");
    fprintf(stderr, "       -i filename      Input FIFO\n");
    fprintf(stderr, "       -o filename      Output FIFO\n");
    fprintf(stderr, "       -p filename      Output PID to filename\n");
}

/*
 * Process the command-line options:
 *      -h -?            Usage screen
 *      -i filename      Input FIFO
 *      -o filename      Output FIFO
 *      -p filename      Output PID to filename
 *
 * @param  argc         main() argc
 * @param  argv         main() argv
 * @return void
 */
static BOOL process_options(int argc, char ** argv) {
    char cmdline_options[] = "h?i:o:p:";
    int opt;

    while ((opt = getopt(argc, argv, cmdline_options)) != -1) {

        switch (opt) {

        case '?':
        case 'h':
            show_usage(argv[0]);
            return FALSE;

        case 'i':
            input_fifo = strdup(optarg);
            break;

        case 'o':
            output_fifo = strdup(optarg);
            break;

        case 'p':
            pid_filename = strdup(optarg);
            break;

        default:
            return FALSE;

        } /* switch (opt) */
    } /* while (...) */

    /* All OK */
    return TRUE;
}

/*
 * Command-line entry point.
 *
 * @param  argc         See parameters at the top of
 * @param  argv         ...this file
 * @return int          See return codes at top of file
 */
int main(int argc, char **argv) {
    int in_fd = 0;
    int out_fd = 0;
    char buffer[LOGGING_MAX_SIZE];
    int buffer_n;
    int rc;

    /* Check arguments */
    if (process_options(argc, argv) != TRUE) {
        /* Bad arguments */
        exit(EXIT_BAD_ARGS);
    }

#ifndef DEBUG

    /* Fork off */
    pid_t child_pid = fork();
    if (child_pid != 0) {
        /* I am the parent, exit */
        exit(0);
    }

#endif /* DEBUG */

    /* Save PID */
    if (pid_filename != NULL) {
        FILE * pid_file = fopen(pid_filename, "w");
        if (pid_file == NULL) {
            log_error("Error opening %s to save PID: %s",
                pid_filename, strerror(errno));
        } else {
            fprintf(pid_file, "%d\n", getpid());
            fclose(pid_file);
        }
    }

    /* Open the two FIFOs */
    in_fd = open(input_fifo, O_RDONLY);
    if (in_fd < 0) {
        log_error("Error opening %s for reading: %s",
            input_fifo, strerror(errno));
        exit(EXIT_OS);
    }

    /* Ignore SIGPIPE */
    if (signal(SIGPIPE, SIG_IGN) == SIG_ERR) {
        log_error("Couldn't unset SIGPIPE signal handler: %s",
            strerror(errno));
        exit(EXIT_OS);
    }

    /* Loop */
    memset(buffer, 0, sizeof(buffer));
    buffer_n = 0;

    for (;;) {

        /* Only read if buffer has room for more bytes */
        if (buffer_n < sizeof(buffer)) {

            rc = read(in_fd, buffer, sizeof(buffer) - buffer_n);
            if (rc < 0) {
                log_error("monitor read() error: %s", strerror(errno));
            } else if (rc == 0) {
                /*
                 * The last writer just closed their FD.  Close and re-open
                 * input_fifo, but wait to emit the last line first.
                 */
                if (buffer_n == 0) {
                    /* Now we can close and re-open it. */
                    if (close(in_fd) < 0) {
                        log_error("Error closing %s: %s",
                            input_fifo, strerror(errno));
                    }

                    in_fd = open(input_fifo, O_RDONLY);
                    if (in_fd < 0) {
                        log_error("Error opening %s for reading: %s",
                            input_fifo, strerror(errno));
                        exit(EXIT_OS);
                    }
                }

            }
        } else {
            /* Pretend we read nothing */
            rc = 0;
        }

        if ((rc > 0) || (buffer_n > 0)) {

            buffer_n += rc;
            if (out_fd == 0) {

                out_fd = open(output_fifo, O_WRONLY | O_NONBLOCK);
                if (out_fd < 0) {
                    if (errno == ENXIO) {
                        /*
                         * ENXIO only occurs when we open the FIFO before ANY
                         * process gets to read from it.  After this one
                         * point we start getting EGAIN and write() returning
                         * 0 to indicate the other side is closed.
                         */

#ifdef DEBUG
                        log_info("No one is reading %s yet", output_fifo);
#endif /* DEBUG */

                        out_fd = 0;
                        buffer_n = 0;
                        continue;
                    }

                    log_error("Error opening %s for writing: %s",
                        output_fifo, strerror(errno));
                    exit(EXIT_OS);
                }
            }

            rc = write(out_fd, buffer, buffer_n);
            if (rc < 0) {
                if (errno == EPIPE) {

#ifdef DEBUG
                    log_info("Reader exited abnormally %s", output_fifo);
#endif /* DEBUG */

                    if (close(out_fd) < 0) {
                        log_error("Error closing %s: %s",
                            output_fifo, strerror(errno));
                    }

                    out_fd = 0;
                    buffer_n = 0;
                    continue;
                }

                if (errno == EAGAIN) {
                    /*
                     * "Resource temporarily unavailable"
                     *
                     * The write() would block.
                     *
                     * This seems to occur when the reader calls close() on
                     * the FIFO.
                     */

#ifdef DEBUG
                    log_info("Reader closed on %s", output_fifo);
#endif /* DEBUG */

                    if (close(out_fd) < 0) {
                        log_error("Error closing %s: %s",
                            output_fifo, strerror(errno));
                    }

                    out_fd = 0;
                    buffer_n = 0;
                    continue;

                }

                log_error("monitor write() error: errno=%d (%s)",
                    errno, strerror(errno));
            } else if (rc == 0) {

#ifdef DEBUG
                log_info("No one is reading %s anymore", output_fifo);
#endif /* DEBUG */

                if (close(out_fd) < 0) {
                    log_error("Error closing %s: %s",
                        output_fifo, strerror(errno));
                }

                out_fd = 0;
                buffer_n = 0;
                continue;

            } else {

#ifdef DEBUG
                log_info("Passed message: %s", buffer);
#endif /* DEBUG */

                /* Shift the buffer down */
                memmove(buffer, buffer + rc, buffer_n - rc);
                buffer_n -= rc;
                memset(buffer + buffer_n, 0, sizeof(buffer) - buffer_n);
            }
        }
    }

    /* Exit OK */
    log_error("Child exits, huh?");
    exit(0);

}
