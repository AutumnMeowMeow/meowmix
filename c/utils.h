/*
 * Written 2022 by Autumn Lamonte ⚧ Trans Liberation Now
 *
 * This is free and unencumbered software released into the public domain.
 *
 * Anyone is free to copy, modify, publish, use, compile, sell, or distribute
 * this software, either in source code form or as a compiled binary, for any
 * purpose, commercial or non-commercial, and by any means.
 *
 * In jurisdictions that recognize copyright laws, the author or authors of
 * this software dedicate any and all copyright interest in the software to
 * the public domain. We make this dedication for the benefit of the public
 * at large and to the detriment of our heirs and successors. We intend this
 * dedication to be an overt act of relinquishment in perpetuity of all
 * present and future rights to this software under copyright law.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
 * THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
 * IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
 * CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *
 * For more information, please refer to <http://unlicense.org/>
 *
 * ----------------------------------------------------------------------------
 *
 * General utilities
 *
 *
 */

#ifndef __utils_h__
#define __utils_h__

#include "common.h"

#include <time.h>       /* time_t */
#include <sys/time.h>   /* struct timeval */

struct default_options_struct {
    BOOL verbose;       /* If TRUE, emit verbose output */

    BOOL quiet;         /* If TRUE, limit output to only errors and don't
                         * call fflush()
                         */

    char * port;        /* /dev/ttyXX port to communicate with */

    char * pid_file;    /* File to write my PID to */

    char * output_file; /* Output file to write to */
};
extern struct default_options_struct global_options;

/* The time a program started up, computed at the END of emit_startup() */
extern time_t starttime;

extern BOOL default_options(int argc, char ** argv);

extern void emit_startup(const char * progname, const char * datestring);
extern void emit_shutdown(const char * progname);

#endif /* __utils_h__ */
