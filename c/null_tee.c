/*
 * Written 2022 by Autumn Lamonte ⚧ Trans Liberation Now
 *
 * This is free and unencumbered software released into the public domain.
 *
 * Anyone is free to copy, modify, publish, use, compile, sell, or distribute
 * this software, either in source code form or as a compiled binary, for any
 * purpose, commercial or non-commercial, and by any means.
 *
 * In jurisdictions that recognize copyright laws, the author or authors of
 * this software dedicate any and all copyright interest in the software to
 * the public domain. We make this dedication for the benefit of the public
 * at large and to the detriment of our heirs and successors. We intend this
 * dedication to be an overt act of relinquishment in perpetuity of all
 * present and future rights to this software under copyright law.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
 * THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
 * IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
 * CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *
 * For more information, please refer to <http://unlicense.org/>
 *
 * ----------------------------------------------------------------------------
 *
 * Selective tee program.
 *
 * This program reads from stdin and writes to a file specified by the -o
 * argument.
 *
 * This plugs into the overall system as a filter to prevent creation of
 * 0-byte files in the logs directory.
 *
 * Invocation:          null_tee { -o output_fifo }
 *
 * Return codes:        0  OK
 *                      20 Out of memory
 *                      21 Error accessing a file
 *                      70 Operating system error
 *                      99 Unknown arguments
 *
 */

#include <errno.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/stat.h>
#include <fcntl.h>
#include "utils.h"      /* For exit codes */

/* Output filename */
static char * output_filename = NULL;

/*
 * Show the usage options:
 *      -h -?            Usage screen
 *      -o filename      Output file
 *
 * @param  progname     Program name
 * @return void
 */
static void show_usage(const char * progname) {
    fprintf(stderr, "USAGE: %s { -h | -? | { -o output_file }\n", progname);
    fprintf(stderr, "       -h -?            Display usage screen\n");
    fprintf(stderr, "       -o filename      Output file\n");
}

/*
 * Process the command-line options:
 *      -h -?            Usage screen
 *      -o filename      Output file
 *
 * @param  argc         main() argc
 * @param  argv         main() argv
 * @return void
 */
static BOOL process_options(int argc, char ** argv) {
    char cmdline_options[] = "h?o:";
    int opt;

    while ((opt = getopt(argc, argv, cmdline_options)) != -1) {

        switch (opt) {

        case '?':
        case 'h':
            show_usage(argv[0]);
            return FALSE;

        case 'o':
            output_filename = strdup(optarg);
            break;

        default:
            return FALSE;

        } /* switch */
    } /* while */

    /* All OK */
    return TRUE;
}

/*
 * Command-line entry point.
 *
 * @param  argc         See parameters at the top of
 * @param  argv         ...this file
 * @return int          See return codes at top of file
 */
int main(int argc, char **argv) {
    int out_fd = 0;
    char buffer[8192];
    int buffer_n;
    int rc;

    /* Check arguments */
    if (process_options(argc, argv) != TRUE) {
        /* Bad arguments */
        exit(EXIT_BAD_ARGS);
    }

    /* Loop */
    memset(buffer, 0, sizeof(buffer));
    buffer_n = 0;

    for (;;) {

        /* Only read if buffer has room for more bytes */
        if (buffer_n < sizeof(buffer)) {

            rc = read(STDIN_FILENO, buffer, sizeof(buffer) - buffer_n);
            if (rc < 0) {
                fprintf(stderr, "null_tee read() error: %s\n", strerror(errno));
            } else if (rc == 0) {
                /*
                 * No bytes read.  Exit.
                 */
                if (buffer_n == 0) {
                    exit(EXIT_OK);
                }

            }
        } else {
            /* Pretend we read nothing */
            rc = 0;
        }

        if ((rc > 0) || (buffer_n > 0)) {

            buffer_n += rc;
            if (out_fd == 0) {

                out_fd = open(output_filename,
                    O_WRONLY | O_CREAT, S_IRUSR | S_IWUSR | S_IRGRP | S_IROTH);
                if (out_fd < 0) {
                    fprintf(stderr, "Error opening %s for writing: %s\n",
                        output_filename, strerror(errno));
                    exit(EXIT_OS);
                }
            }

            rc = write(out_fd, buffer, buffer_n);
            if (rc <= 0) {
                if (close(out_fd) < 0) {
                    fprintf(stderr, "Error closing %s: %s\n", output_filename,
                        strerror(errno));
                }
                fprintf(stderr, "monitor write() error: errno=%d (%s)\n",
                    errno, strerror(errno));
                exit(EXIT_OS);
            } else {
                /* Shift the buffer down */
                memmove(buffer, buffer + rc, buffer_n - rc);
                buffer_n -= rc;
                memset(buffer + buffer_n, 0, sizeof(buffer) - buffer_n);
            }
        }
    }

    /* Exit OK */
    exit(EXIT_OK);
}
