/*
 * Written 2022 by Autumn Lamonte ⚧ Trans Liberation Now
 *
 * This is free and unencumbered software released into the public domain.
 *
 * Anyone is free to copy, modify, publish, use, compile, sell, or distribute
 * this software, either in source code form or as a compiled binary, for any
 * purpose, commercial or non-commercial, and by any means.
 *
 * In jurisdictions that recognize copyright laws, the author or authors of
 * this software dedicate any and all copyright interest in the software to
 * the public domain. We make this dedication for the benefit of the public
 * at large and to the detriment of our heirs and successors. We intend this
 * dedication to be an overt act of relinquishment in perpetuity of all
 * present and future rights to this software under copyright law.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
 * THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
 * IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
 * CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *
 * For more information, please refer to <http://unlicense.org/>
 *
 * ----------------------------------------------------------------------------
 *
 * Serial passthru program.
 *
 *
 * Invocation:          serial_passthru baud,bits,parity,stop[,xonxoff]
 *
 * Return codes:        0  OK
 *                      99 Unknown arguments
 *
 */

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <errno.h>
#include <fcntl.h>
#include <signal.h>
#include <string.h>
#include <termios.h>
#include <assert.h>
#include "common.h"

/*
 * Global stay-alive flag.  It is set to FALSE by the SIGUSR1 and
 * SIGTERM signal handlers.
 */
static BOOL stay_alive = TRUE;

/* The original SIGTERM handler */
static void (*old_sigterm)(int);

/* The original SIGINT handler */
static void (*old_sigint)(int);

/* The original SIGUSR1 handler */
static void (*old_sigusr1)(int);

/*
 * SIGTERM signal handler
 */
static void sigterm_handler(int s) {
    fprintf(stdout, "SIGTERM seen, switching stay_alive flag to FALSE...\n");
    stay_alive = FALSE;
}

/*
 * SIGINT signal handler
 */
static void sigint_handler(int s) {
    fprintf(stdout, "SIGINT\n");
}

/*
 * SIGUSR1 signal handler
 */
static void sigusr1_handler(int s) {
    fprintf(stdout, "SIGUSR1 seen, switching stay_alive flag to FALSE...\n");
    stay_alive = FALSE;
}

/*
 * Command-line entry point.
 *
 * @param  argc         See parameters at the top of
 * @param  argv         ...this file
 * @return int          See return codes at top of file
 */
int main(int argc, char **argv) {
    unsigned char serialbuf[MAX_SERIAL_WRITE];
    unsigned int serialbuf_n = 0;
    unsigned char userbuf[MAX_SERIAL_WRITE];
    unsigned int userbuf_n = 0;
    int i;
    int ctrl_x = 0;

    int rc = EXIT_OK;
    int fd;
    fd_set readfds;
    fd_set writefds;
    fd_set exceptfds;
    struct timeval select_timeout;

    char * port;
    char * baud;
    char * bits;
    char * parity;
    char * stop_bits;
    char * xonxoff;
    char * begin;

    /* struct termios used to set the serial port communication parameters */
    struct termios old_termios;
    struct termios new_termios;

    /* struct termios used to set the stdin parameters */
    struct termios stdin_old_termios;
    struct termios stdin_new_termios;

    /* How long we will allow each poll/select to wait before seeing data */
    struct timeval polling_timeout;

    /* Set poll/select timeout */
    polling_timeout.tv_sec = 0;             /* 0 seconds */

    polling_timeout.tv_usec = 500000;       /* 500,000 microseconds = 500
                                             * milliseconds
                                             */

    /* Check arguments */
    if (argc != 3) {
        fprintf(stderr, "USAGE: serial_passthru port baud,bits,parity,stop[,xonxoff]\n");
        exit(EXIT_BAD_ARGS);
    }

    port = argv[1];

    fprintf(stdout, "Port:      %s\n", port);

    /* Break parameters down */
    begin = argv[2];

    /* baud */
    baud = begin;
    begin = strchr(begin + 1, ',');
    if (begin == NULL) {
        fprintf(stderr, "Parameter string incorrect.\n");
        exit(EXIT_BAD_ARGS);
    }
    *begin = 0;
    begin++;
    if (*begin == 0) {
        fprintf(stderr, "Parameter string incorrect.\n");
        exit(EXIT_BAD_ARGS);
    }

    /* bits */
    bits = begin;
    begin = strchr(begin + 1, ',');
    if (begin == NULL) {
        fprintf(stderr, "Parameter string incorrect.\n");
        exit(EXIT_BAD_ARGS);
    }
    *begin = 0;
    begin++;
    if (*begin == 0) {
        fprintf(stderr, "Parameter string incorrect.\n");
        exit(EXIT_BAD_ARGS);
    }

    /* parity */
    parity = begin;
    begin = strchr(begin + 1, ',');
    if (begin == NULL) {
        fprintf(stderr, "Parameter string incorrect.\n");
        exit(EXIT_BAD_ARGS);
    }
    *begin = 0;
    begin++;
    if (*begin == 0) {
        fprintf(stderr, "Parameter string incorrect.\n");
        exit(EXIT_BAD_ARGS);
    }
    /* stop_bits */
    stop_bits = begin;
    begin = strchr(begin + 1, ',');

    if (begin != NULL) {
        /* xonxoff */
        *begin = 0;
        begin++;
        xonxoff = begin;
        if (*begin == 0) {
            fprintf(stderr, "Parameter string incorrect.\n");
            exit(EXIT_BAD_ARGS);
        }
    } else {
        xonxoff = "";
    }

    fprintf(stdout, "baud:      %s\n", baud);
    fprintf(stdout, "bits:      %s\n", bits);
    fprintf(stdout, "parity:    %s\n", parity);
    fprintf(stdout, "stop_bits: %s\n", stop_bits);
    fprintf(stdout, "xonxoff:   %s\n", xonxoff);

    /* Set the SIGTERM signal handler */
    old_sigterm = signal(SIGTERM, &sigterm_handler);
    if (old_sigterm == SIG_ERR) {
        fprintf(stderr, "Couldn't change SIGTERM signal handler: %s\n",
            strerror(errno));
        exit(EXIT_OS);
    }

    /* Set the SIGINT signal handler */
    old_sigint = signal(SIGINT, &sigint_handler);
    if (old_sigint == SIG_ERR) {
        fprintf(stderr, "Couldn't change SIGINT signal handler: %s\n",
            strerror(errno));
        exit(EXIT_OS);
    }

    /* Set the SIGUSR1 signal handler */
    old_sigusr1 = signal(SIGUSR1, &sigusr1_handler);
    if (old_sigusr1 == SIG_ERR) {
        fprintf(stderr, "Couldn't change SIGUSR1 signal handler: %s\n",
            strerror(errno));
        exit(EXIT_OS);
    }

    /* Get access to serial port */
    fd = open(port, O_RDWR | O_NOCTTY | O_SYNC | O_NONBLOCK);
    if (fd < 0) {
        fprintf(stderr, "Error opening %s: %s (%d)\n", port, strerror(errno),
            errno);
        rc = EXIT_SERIAL_PORT;
        goto the_end;
    }

    /* Flush the input buffer */
    if (tcflush(fd, TCIOFLUSH) < 0) {
        fprintf(stderr, "tcflush() on %s failed: %s (%d)\n", port,
            strerror(errno), errno);
        rc = EXIT_SERIAL_PORT;
        goto the_end;
    }

    /* Setup stdin */
    if (tcgetattr(STDIN_FILENO, &stdin_old_termios) < 0) {
        fprintf(stderr, "tcgetattr() on stdin failed: %s\n", strerror(errno));
        rc = EXIT_SERIAL_PORT;
        goto the_end;
    }
    memcpy(&stdin_new_termios, &stdin_old_termios, sizeof(struct termios));
    cfmakeraw(&stdin_new_termios);
    if (tcsetattr(STDIN_FILENO, TCSANOW, &stdin_new_termios) < 0) {
        fprintf(stderr, "tcsetattr() on stdin failed: %s\n", strerror(errno));
        rc = EXIT_SERIAL_PORT;
        goto the_end;
    }

    if (tcgetattr(fd, &old_termios) < 0) {
        fprintf(stderr, "tcgetattr() on %s failed: %s\n", port,
            strerror(errno));
        rc = EXIT_SERIAL_PORT;
        goto the_end;
    }
    memcpy(&new_termios, &old_termios, sizeof(struct termios));
    cfmakeraw(&new_termios);
    /* These parameters taken from minicom.  Seems to work for them. */
    new_termios.c_iflag =  IGNBRK;
    new_termios.c_lflag = 0;
    new_termios.c_oflag = 0;
    new_termios.c_cflag |= CLOCAL | CREAD;
    new_termios.c_cflag &= ~CRTSCTS;

    int speed = B9600;
    if (strcmp(baud, "115200") == 0) speed = B115200;
    if (strcmp(baud, "57600") == 0) speed = B57600;
    if (strcmp(baud, "38400") == 0) speed = B38400;
    if (strcmp(baud, "19200") == 0) speed = B19200;
    if (strcmp(baud, "9600") == 0) speed = B9600;
    if (strcmp(baud, "4800") == 0) speed = B4800;
    if (strcmp(baud, "2400") == 0) speed = B2400;
    if (strcmp(baud, "1200") == 0) speed = B1200;
    if (strcmp(baud, "300") == 0) speed = B300;

    if (cfsetispeed(&new_termios, speed) < 0) {
        fprintf(stderr, "cfsetispeed() on %s failed: %s\n", port,
            strerror(errno));
        rc = EXIT_SERIAL_PORT;
        goto the_end;
    }
    if (cfsetospeed(&new_termios, speed) < 0) {
        fprintf(stderr, "cfsetospeed() on %s failed: %s\n", port,
            strerror(errno));
        rc = EXIT_SERIAL_PORT;
        goto the_end;
    }

    if (strcmp(bits, "8") != 0) {
        new_termios.c_cflag &= ~CSIZE;

        if (strcmp(bits, "7") == 0)
            new_termios.c_cflag |= CS7;

        if (strcmp(bits, "6") == 0)
            new_termios.c_cflag |= CS6;

        if (strcmp(bits, "5") == 0)
            new_termios.c_cflag |= CS5;
    }

    if (strcasecmp(parity, "e") == 0) {
        new_termios.c_cflag |= PARENB;
    }
    if (strcasecmp(parity, "o") == 0) {
        new_termios.c_cflag |= PARENB | PARODD;
    }

    if (strcmp(stop_bits, "2") == 0) {
        new_termios.c_cflag |= CSTOPB;
    } else {
        new_termios.c_cflag &= ~CSTOPB;
    }
    if (strcmp(xonxoff, "xonxoff") == 0) {
        new_termios.c_iflag |= IXON | IXOFF;
        new_termios.c_oflag |= IXON | IXOFF;
    }

    if (tcsetattr(fd, TCSANOW, &new_termios) < 0) {
        fprintf(stderr, "tcsetattr() on %s failed: %s\n", port,
            strerror(errno));
        rc = EXIT_SERIAL_PORT;
        goto the_end;
    }

    fprintf(stdout, "------ BEGIN (Press ^X^X^X^X to end) ------\r\n");

    for (;;) {

        if (stay_alive == FALSE) {
            break;
        }

        FD_ZERO(&readfds);
        FD_ZERO(&writefds);
        FD_ZERO(&exceptfds);
        FD_SET(fd, &readfds);
        FD_SET(fd, &writefds);
        FD_SET(STDIN_FILENO, &readfds);
        FD_SET(STDOUT_FILENO, &writefds);

        memcpy(&select_timeout, &polling_timeout, sizeof(struct timeval));
        rc = select(fd+1, &readfds, &writefds, &exceptfds, &select_timeout);
        if (rc == 0) {
            /* Timeout */
            continue;
        }
        if (rc < 0) {
            /* Error in select() call */
            fprintf(stderr, "Error in select() call on %s: %s (%d)\n", port,
                strerror(errno), errno);
            rc = EXIT_SERIAL_PORT;
            goto the_end;
        }

        if (FD_ISSET(fd, &readfds) && (serialbuf_n < sizeof(serialbuf))) {
            /* I can read from the serial port */
            rc = read(fd, serialbuf + serialbuf_n,
                sizeof(serialbuf) - serialbuf_n);
            if (rc < 0) {
                /* Error */
                fprintf(stderr, "Error in read() call on %s: %s (%d)\n", port,
                    strerror(errno), errno);
                rc = EXIT_SERIAL_PORT;
                goto the_end;
            }
            if (rc == 0) {
                /* Remote end closed connection, huh? */
                fprintf(stderr, "read() call on %s returned 0, huh?\n", port);
                rc = EXIT_SERIAL_PORT;
                goto the_end;
            }
            serialbuf_n += rc;
        }
        if (FD_ISSET(fd, &writefds) && (userbuf_n > 0)) {
            /* I can write to the serial port */
            rc = write(fd, userbuf, userbuf_n);
            if (rc < 0) {
                /* Error */
                fprintf(stderr, "Error in write() call on %s: %s (%d)\n", port,
                    strerror(errno), errno);
                rc = EXIT_SERIAL_PORT;
                goto the_end;
            }
            memmove(userbuf, userbuf + rc, rc);
            userbuf_n -= rc;
        }
        if (FD_ISSET(STDIN_FILENO, &readfds) && (userbuf_n < sizeof(userbuf))) {
            /* I can read from stdin */
            rc = read(STDIN_FILENO, userbuf + userbuf_n,
                sizeof(userbuf) - userbuf_n);
            if (rc < 0) {
                /* Error */
                fprintf(stderr, "Error in read() call on stdin: %s (%d)\n",
                    strerror(errno), errno);
                rc = EXIT_SERIAL_PORT;
                goto the_end;
            }
            if (rc == 0) {
                /* Remote end closed connection, huh? */
                fprintf(stderr, "read() call on stdin returned 0, huh?\n");
                rc = EXIT_SERIAL_PORT;
                goto the_end;
            }

            /* Scan for ^X */
            for (i = 0; i < rc; i++) {
                if (userbuf[userbuf_n + i] != 0x18) {
                    ctrl_x = 0;
                } else {
                    /* Drop it from the output stream */
                    memmove(userbuf + i, userbuf + i + 1, 1);
                    rc--;
                    i--;
                    ctrl_x++;
                    if (ctrl_x == 4) {
                        stay_alive = FALSE;
                    }
                }
            }

            userbuf_n += rc;
        }
        if (FD_ISSET(STDOUT_FILENO, &writefds) && (serialbuf_n > 0)) {
            /* I can write to stdout */
            rc = write(STDOUT_FILENO, serialbuf, serialbuf_n);
            if (rc < 0) {
                /* Error */
                fprintf(stderr, "Error in write() call on stdout: %s (%d)\n",
                    strerror(errno), errno);
                rc = EXIT_SERIAL_PORT;
                goto the_end;
            }
            memmove(serialbuf, serialbuf + rc, rc);
            serialbuf_n -= rc;
        }

    } /* for (;;) */

the_end:

    fprintf(stdout, "------ END ------\r\n");

    /* Release the serial port */
    if (fd > 0) {
        close(fd);
    }

    /* Restore state of stdin */
    if (tcsetattr(STDIN_FILENO, TCSANOW, &stdin_old_termios) < 0) {
        fprintf(stderr, "tcsetattr() on stdin failed: %s\n", strerror(errno));
        rc = EXIT_SERIAL_PORT;
        goto the_end;
    }

    /* Exit out */
    exit(rc);
}
