/*
 * Written 2022 by Autumn Lamonte ⚧ Trans Liberation Now
 *
 * This is free and unencumbered software released into the public domain.
 *
 * Anyone is free to copy, modify, publish, use, compile, sell, or distribute
 * this software, either in source code form or as a compiled binary, for any
 * purpose, commercial or non-commercial, and by any means.
 *
 * In jurisdictions that recognize copyright laws, the author or authors of
 * this software dedicate any and all copyright interest in the software to
 * the public domain. We make this dedication for the benefit of the public
 * at large and to the detriment of our heirs and successors. We intend this
 * dedication to be an overt act of relinquishment in perpetuity of all
 * present and future rights to this software under copyright law.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
 * THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
 * IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
 * CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *
 * For more information, please refer to <http://unlicense.org/>
 *
 * ----------------------------------------------------------------------------
 *
 * General utilities
 *
 *
 */

#include <stdlib.h>
#include <string.h>
#include <errno.h>
#include <fcntl.h>
#include <sys/stat.h>
#include <signal.h>
#include <unistd.h>
#include <math.h>
#include <assert.h>
#include "utils.h"
#include "logging.h"

struct default_options_struct global_options = {
    FALSE,
    FALSE,
    NULL
};

/* The time a program started up, computed at the END of emit_startup() */
time_t starttime;

/*
 * Show the default startup header
 *
 * @param  progname     Program name
 * @return void
 */
void emit_startup(const char * progname, const char * datestring) {
    time_t now;
    char * current_time;
    FILE * pid_file;

    /* Startup message */
    now = time(NULL);
    current_time = ctime(&now);
    current_time[strlen(current_time)-1] = 0;
    log_info("%s: starting up, PID is %d, PPID is %d, local time is %s...",
        progname, getpid(), getppid(), current_time);
    log_info("(Compiled on %s)", datestring);

    /* Show basic options */
    if (global_options.pid_file != NULL) {
        log_info("PID TO:         %s", global_options.pid_file);
        /* Save PID to PID file */
        pid_file = fopen(global_options.pid_file, "w");
        if (pid_file == NULL) {
            log_error("Error opening %s to save PID: %s",
                global_options.pid_file, strerror(errno));
        } else {
            fprintf(pid_file, "%d\n", getpid());
            fclose(pid_file);
        }
    }

    log_info("Port:           %s", global_options.port);
    log_info("Output to:      %s", global_options.output_file);

    /* Compute starttime */
    starttime = time(NULL);
}

/*
 * Show the default shutdown footer
 *
 * @param  progname     Program name
 * @return void
 */
void emit_shutdown(const char * progname) {
    time_t now;
    char * current_time;

    /* Shutdown message */
    now = time(NULL);
    current_time = ctime(&now);
    current_time[strlen(current_time)-1] = 0;
    log_info("%s: shutting down, PID is %d, PPID is %d, local time is %s...",
        progname, getpid(), getppid(), current_time);

    if (global_options.pid_file != NULL) {
        log_info("Removing %s...", global_options.pid_file);

        if (unlink(global_options.pid_file) < 0) {
            log_error("Error removing %s: %s", global_options.pid_file,
                strerror(errno));
        }
    }
}

/*
 * Show the default usage options:
 *      -h -?            Usage screen
 *      -p /dev/ttyXX    Port
 *      -o filename      Output file
 *      -i filename      Filename to output my PID to
 *      -q               Power down on exit
 *      -c               Run in continuous mode
 *      -v               Verbose output
 *      -x               Quiet output
 *
 * @param  progname	Program name
 * @return void
 */
static void default_usage(const char * progname) {
    fprintf(stderr, "USAGE: %s { -h | -? | { -p /dev/ttyXX } [ -v ] }\n", progname);
    fprintf(stderr, "       -h or -?         Display usage screen\n");
    fprintf(stderr, "       -p /dev/ttyXX    Port to communicate with device\n");
    fprintf(stderr, "       -o filename      Filename to output to\n");
    fprintf(stderr, "       -i filename      Filename to output my PID to\n");
    fprintf(stderr, "       -v               Verbose output\n");
    fprintf(stderr, "       -x               Quiet output\n");
}

/*
 * Process the default options:
 *      -h -?            Usage screen
 *      -p /dev/ttyXX    Port
 *      -o filename      Output file
 *      -v               Verbose output
 *      -x               Quiet output
 *
 * @param  argc         main() argc
 * @param  argv         main() argv
 * @return void
 */
BOOL default_options(int argc, char ** argv) {
    char cmdline_options[] = "h?p:o:vxi:";
    int opt;

    while ((opt = getopt(argc, argv, cmdline_options)) != -1) {

        switch (opt) {

        case '?':
        case 'h':
            default_usage(argv[0]);
            return FALSE;

        case 'p':
            global_options.port = strdup(optarg);
            break;

        case 'o':
            global_options.output_file = strdup(optarg);
            break;

        case 'i':
            global_options.pid_file = strdup(optarg);
            break;

        case 'v':
            global_options.verbose = TRUE;
            assert(global_options.quiet == FALSE);
            break;

        case 'x':
            global_options.quiet = TRUE;
            assert(global_options.verbose == FALSE);
            break;

        default:
            return FALSE;

        } /* switch */
    } /* while */

    /* All OK */
    return TRUE;
}
