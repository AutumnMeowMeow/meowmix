/*
 * Written 2022 by Autumn Lamonte ⚧ Trans Liberation Now
 *
 * This is free and unencumbered software released into the public domain.
 *
 * Anyone is free to copy, modify, publish, use, compile, sell, or distribute
 * this software, either in source code form or as a compiled binary, for any
 * purpose, commercial or non-commercial, and by any means.
 *
 * In jurisdictions that recognize copyright laws, the author or authors of
 * this software dedicate any and all copyright interest in the software to
 * the public domain. We make this dedication for the benefit of the public
 * at large and to the detriment of our heirs and successors. We intend this
 * dedication to be an overt act of relinquishment in perpetuity of all
 * present and future rights to this software under copyright law.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
 * THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
 * IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
 * CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *
 * For more information, please refer to <http://unlicense.org/>
 *
 * ----------------------------------------------------------------------------
 *
 * Common macros
 *
 *
 */

#ifndef __common_h__
#define __common_h__

#ifndef BOOL
#define BOOL int
#endif
#ifndef TRUE
#define TRUE 1
#endif
#ifndef FALSE
#define FALSE 0
#endif

#define LOGGING_MAX_SIZE	(MAX_SERIAL_WRITE * 5)
#define TIMESTAMP_MAX_SIZE	256

/* The maximum number of bytes to write to the serial port at one time */
#define MAX_SERIAL_WRITE	2048

/* External exit codes */
#define EXIT_OK			0	/* All OK */

#define EXIT_SERIAL_PORT	5	/* Couldn't access a /dev/ttyXX
                                         * serial port
                                         */

#define EXIT_SERIAL_TIMEOUT	6	/* Timed out while reading a
                                         * /dev/ttyXX serial port
                                         */

#define EXIT_MEMORY		20	/* Out of memory */

#define EXIT_FILE_IO		21	/* Couldn't open a file */

#define EXIT_OS			70	/* Couldn't perform a routine OS call
                                         * (e.g. signal())
                                         */

#define EXIT_BAD_ARGS		99	/* Bad arguments */
#define EXIT_ERROR		101	/* General catch-all */

/* Internal function return codes */
#define OK			0
#define ERROR			-1
#define TIMEOUT			1

#define MAKE_DATE_STRING(X, Y)	(X " " Y)

#endif /* __common_h__ */
