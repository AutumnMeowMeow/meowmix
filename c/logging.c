/*
 * Written 2022 by Autumn Lamonte ⚧ Trans Liberation Now
 *
 * This is free and unencumbered software released into the public domain.
 *
 * Anyone is free to copy, modify, publish, use, compile, sell, or distribute
 * this software, either in source code form or as a compiled binary, for any
 * purpose, commercial or non-commercial, and by any means.
 *
 * In jurisdictions that recognize copyright laws, the author or authors of
 * this software dedicate any and all copyright interest in the software to
 * the public domain. We make this dedication for the benefit of the public
 * at large and to the detriment of our heirs and successors. We intend this
 * dedication to be an overt act of relinquishment in perpetuity of all
 * present and future rights to this software under copyright law.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
 * THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
 * IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
 * CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *
 * For more information, please refer to <http://unlicense.org/>
 *
 *
 * ----------------------------------------------------------------------------
 *
 * Logging API
 *
 * This provides a uniform API for logging output at debug, info, warn, and
 * error levels.
 *
 */

#include <stdarg.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "utils.h"
#include "logging.h"

/*
 * Log a message at the SERIAL_READ level
 *
 * @param  format       Format string
 * @param  ...          Other arguments
 * @return void
 */
void log_serial_read(const char * format, ...) {
    va_list arglist;
    char outbuf[LOGGING_MAX_SIZE];
    char timestring[TIMESTAMP_MAX_SIZE];
    struct tm * timestamp;
    struct timeval now;

    gettimeofday(&now, NULL);
    timestamp = gmtime(&now.tv_sec);
    strftime(timestring, sizeof(timestring), "[%Y-%m-%d %H:%M:%S.%%03d] SR ",
        timestamp);
    sprintf(outbuf, timestring, (now.tv_usec / 1000));

    va_start(arglist, format);
    vsprintf((char *)(outbuf + strlen(outbuf)), format, arglist);
    va_end(arglist);
    fprintf(stdout, outbuf);
    fprintf(stdout, "\n");

    /* Only flush if we're in DEBUG mode */
    if (global_options.verbose == TRUE) {
        fflush(stdout);
    }

}

/*
 * Log a message at the SERIAL_WRITE level
 *
 * @param  format       Format string
 * @param  ...          Other arguments
 * @return void
 */
void log_serial_write(const char * format, ...) {
    va_list arglist;
    char outbuf[LOGGING_MAX_SIZE];
    char timestring[TIMESTAMP_MAX_SIZE];
    struct tm * timestamp;
    struct timeval now;

    gettimeofday(&now, NULL);
    timestamp = gmtime(&now.tv_sec);
    strftime(timestring, sizeof(timestring), "[%Y-%m-%d %H:%M:%S.%%03d] SW ",
        timestamp);
    sprintf(outbuf, timestring, (now.tv_usec / 1000));

    va_start(arglist, format);
    vsprintf((char *)(outbuf + strlen(outbuf)), format, arglist);
    va_end(arglist);
    fprintf(stdout, outbuf);
    fprintf(stdout, "\n");

    /* Only flush if we're in DEBUG mode */
    if (global_options.verbose == TRUE) {
        fflush(stdout);
    }

}

/*
 * Log a message at the DEBUG level
 *
 * @param  format       Format string
 * @param  ...          Other arguments
 * @return void
 */
void log_debug(const char * format, ...) {
    va_list arglist;
    char outbuf[LOGGING_MAX_SIZE];
    char timestring[TIMESTAMP_MAX_SIZE];
    struct tm * timestamp;
    struct timeval now;

    /* Only show debug when passed -v verbose parameter */
    if (global_options.verbose == FALSE) {
        return;
    }

    gettimeofday(&now, NULL);
    timestamp = gmtime(&now.tv_sec);
    strftime(timestring, sizeof(timestring), "[%Y-%m-%d %H:%M:%S.%%03d] D ",
        timestamp);
    sprintf(outbuf, timestring, (now.tv_usec / 1000));

    va_start(arglist, format);
    vsprintf((char *)(outbuf + strlen(outbuf)), format, arglist);
    va_end(arglist);
    fprintf(stdout, outbuf);
    fprintf(stdout, "\n");

    /* Only flush if we're in DEBUG mode */
    if (global_options.verbose == TRUE) {
        fflush(stdout);
    }

}

/*
 * Log a message at the INFO level
 *
 * @param  format       Format string
 * @param  ...          Other arguments
 * @return void
 */
void log_info(const char * format, ...) {
    va_list arglist;
    char outbuf[LOGGING_MAX_SIZE];
    char timestring[TIMESTAMP_MAX_SIZE];
    struct tm * timestamp;
    struct timeval now;

    gettimeofday(&now, NULL);
    timestamp = gmtime(&now.tv_sec);
    strftime(timestring, sizeof(timestring), "[%Y-%m-%d %H:%M:%S.%%03d] I ",
        timestamp);
    sprintf(outbuf, timestring, (now.tv_usec / 1000));

    va_start(arglist, format);
    vsprintf((char *)(outbuf + strlen(outbuf)), format, arglist);
    va_end(arglist);
    fprintf(stdout, outbuf);
    fprintf(stdout, "\n");

    /* Only flush if we're in DEBUG mode */
    if (global_options.verbose == TRUE) {
        fflush(stdout);
    }

}

/*
 * Log a message at the WARN level
 *
 * @param  format       Format string
 * @param  ...          Other arguments
 * @return void
 */
void log_warn(const char * format, ...) {
    va_list arglist;
    char outbuf[LOGGING_MAX_SIZE];
    char timestring[TIMESTAMP_MAX_SIZE];
    struct tm * timestamp;
    struct timeval now;

    gettimeofday(&now, NULL);
    timestamp = gmtime(&now.tv_sec);
    strftime(timestring, sizeof(timestring), "[%Y-%m-%d %H:%M:%S.%%03d] W ",
        timestamp);
    sprintf(outbuf, timestring, (now.tv_usec / 1000));

    va_start(arglist, format);
    vsprintf((char *)(outbuf + strlen(outbuf)), format, arglist);
    va_end(arglist);
    fprintf(stdout, outbuf);
    fprintf(stdout, "\n");

    /* Only flush if we're in DEBUG mode */
    if (global_options.verbose == TRUE) {
        fflush(stdout);
    }

}

/*
 * Log a message at the ERROR level
 *
 * @param  format       Format string
 * @param  ...          Other arguments
 * @return void
 */
void log_error(const char * format, ...) {
    va_list arglist;
    char outbuf[LOGGING_MAX_SIZE];
    char timestring[TIMESTAMP_MAX_SIZE];
    struct tm * timestamp;
    struct timeval now;

    gettimeofday(&now, NULL);
    timestamp = gmtime(&now.tv_sec);
    strftime(timestring, sizeof(timestring), "[%Y-%m-%d %H:%M:%S.%%03d] E ",
        timestamp);
    sprintf(outbuf, timestring, (now.tv_usec / 1000));

    va_start(arglist, format);
    vsprintf((char *)(outbuf + strlen(outbuf)), format, arglist);
    va_end(arglist);

    fprintf(stderr, outbuf);
    fprintf(stderr, "\n");
    /* We always flush on an error */
    fflush(stderr);

    fprintf(stdout, outbuf);
    fprintf(stdout, "\n");
    /* We always flush on an error */
    fflush(stdout);

}
