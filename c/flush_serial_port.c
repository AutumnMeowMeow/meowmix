/*
 * Written 2022 by Autumn Lamonte ⚧ Trans Liberation Now
 *
 * This is free and unencumbered software released into the public domain.
 *
 * Anyone is free to copy, modify, publish, use, compile, sell, or distribute
 * this software, either in source code form or as a compiled binary, for any
 * purpose, commercial or non-commercial, and by any means.
 *
 * In jurisdictions that recognize copyright laws, the author or authors of
 * this software dedicate any and all copyright interest in the software to
 * the public domain. We make this dedication for the benefit of the public
 * at large and to the detriment of our heirs and successors. We intend this
 * dedication to be an overt act of relinquishment in perpetuity of all
 * present and future rights to this software under copyright law.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
 * THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
 * IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
 * CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *
 * For more information, please refer to <http://unlicense.org/>
 *
 * ----------------------------------------------------------------------------
 *
 * Serial port flusher
 *
 * This is a standalone main() to wipe out the buffer of a serial port.  It
 * accepts the standard arguments -v, -h, -o, and -p.
 *
 * Invocation:          flush_serial_port { { -p /dev/ttyXX } [ -v ] | -h | -? }
 *
 * Return codes:        0  OK
 *                      99 Unknown arguments
 *
 */

#include <stdlib.h>
#include <errno.h>
#include <fcntl.h>
#include <string.h>
#include <assert.h>
#include <termios.h>
#include <unistd.h>
#include <sys/stat.h>
#include <sys/wait.h>
#include "logging.h"
#include "utils.h"

#define MY_NAME         "FLUSH_PORT"

struct port_struct {
    int fd;             /* File descriptor */
    char * filename;    /* File name (/dev/ttyXX) */
};
#define reset_port(A) \
        A.fd            = -1; \
        A.filename      = "RESET";

struct port_struct serial_port = {
        -1, "INIT"
};

/* If TRUE, call log_serial_read / log_serial_write */
#define DEBUG_SERIAL_IO TRUE

/*
 * Read data from the serial port and put into buffer, starting at
 * buffer[buffer_start] and reading no more than buffer[buffer_max].
 * The number of NEW bytes read is ADDED to buffer_n.
 *
 * @param  buffer       The buffer to write data to
 * @param  buffer_max   The TOTAL SIZE of the buffer (e.g. sizeof(x))
 * @param  buffer_start The index into buffer to append data
 * @param  buffer_n     The total number of bytes in the buffer (e.g. strlen(x))
 * @param  timeout      How long to wait in the poll/select call
 *
 * @param  expect_zero_sometimes        If TRUE, when read() returns zero
 *                                      return TIMEOUT instead of error.
 *                                      Needed for usbserial driver.
 *
 * @return status       OK means bytes were read and appended to the buffer
 *                      ERROR means an error occurred either in the poll or
 *                      ...the read
 *                      TIMEOUT means NO bytes were read because the timeout
 *                      expired
 */
static int read_serial_port(unsigned char * buffer, int buffer_max,
    int buffer_start, int * buffer_n, struct timeval * timeout,
    BOOL expect_zero_sometimes) {

    char line[MAX_SERIAL_WRITE * 5];
    int i;
    int rc;
    struct timeval select_timeout;
    fd_set readfds;
    fd_set writefds;
    fd_set exceptfds;

    assert(buffer != NULL);
    assert(buffer_n != NULL);
    assert(*buffer_n >= 0);
    assert(timeout != NULL);
    assert(buffer_start >= 0);
    assert(buffer_max <= MAX_SERIAL_WRITE);
    assert(buffer_max > buffer_start);

    FD_ZERO(&readfds);
    FD_ZERO(&writefds);
    FD_ZERO(&exceptfds);
    FD_SET(serial_port.fd, &readfds);

    memcpy(&select_timeout, timeout, sizeof(struct timeval));
    rc = select(serial_port.fd + 1, &readfds, &writefds, &exceptfds,
        &select_timeout);

    if (rc < 0) {
        if (errno == EINTR) {
            /* Interrupted by a signal */
            log_warn("EINTR in select() call on %s, returning TIMEOUT: %s",
                serial_port.filename, strerror(errno));
            return TIMEOUT;
        }

        /* Error */
        log_error("Error in select() call on %s: %s (%d)",
            serial_port.filename, strerror(errno), errno);
        return ERROR;
    }
    if (rc == 0) {
        /* Timed out */
        return TIMEOUT;
    }

    /* Read the data */
    rc = read(serial_port.fd, buffer + buffer_start, buffer_max - (*buffer_n));
    if (rc < 0) {
        /* Error */
        log_error("Error in read() call on %s: %s (%d)",
            serial_port.filename, strerror(errno), errno);
        return ERROR;
    }
    if (rc == 0) {
        if (expect_zero_sometimes == TRUE) {
            /*
             * My caller knows that this device might return zero on read().
             * By the contract of select(), this should NEVER happen, but
             * maybe I'll need it later?  Return TIMEOUT instead of error.
             */
            return TIMEOUT;
        }

        /* This might happen on a serial port when two processes try to
         * access it simultaneously. */
        log_error("Remote for %s closed the connection, huh?",
            serial_port.filename);
        return ERROR;
    }
    (*buffer_n) += rc;

    if ((DEBUG_SERIAL_IO == TRUE) && (global_options.verbose == TRUE)) {

        memset(line, 0, sizeof(line));
        for (i=0; i < (*buffer_n) - buffer_start; i++) {
            if ((buffer[buffer_start + i] >= 0x20)
                && (buffer[buffer_start + i] <= 0x7E)
            ) {
                /* Printable character, special case '\' and '%' */
                if (buffer[buffer_start + i] == '\\') {
                    snprintf(line + strlen(line), sizeof(line) - strlen(line),
                        "\\\\");
                } else if (buffer[buffer_start + i] == '%') {
                    snprintf(line + strlen(line), sizeof(line) - strlen(line),
                        "\\045");
                } else {
                    snprintf(line + strlen(line), sizeof(line) - strlen(line),
                        "%c", buffer[buffer_start + i]);
                }
            } else {
                snprintf(line + strlen(line), sizeof(line) - strlen(line),
                    "\\%03o", buffer[buffer_start + i]);
            }
        }
        log_serial_read("%04d %s", (*buffer_n) - buffer_start, line);

    } /* if (DEBUG_SERIAL_IO == TRUE) */

    /* All is well */
    return OK;
}

/* For completeness, the flush and write functions are defined. */
#if 0

/*
 * Flush data from the serial port.
 *
 * @param  timeout      How long to wait in the poll/select call
 *
 * @return status       EXIT_OK means bytes were read and appended to the buffer
 *                      EXIT_SERIAL_TIMEOUT means an error occurred
 */
static int flush_serial_port(float timeout) {
    unsigned char buffer[1024];
    int buffer_n;
    int buffer_before;
    int rc;

    /* How long we will allow each poll/select to wait before seeing data */
    struct timeval polling_timeout;

    /* Set poll/select timeout */
    polling_timeout.tv_sec = timeout;
    polling_timeout.tv_usec = (unsigned long)(timeout * 1000000.0f) % 1000000;

    log_info("Flushing data from %s...", serial_port.filename);

    buffer_n = 0;
    buffer_before = buffer_n;
    while ((rc = read_serial_port(buffer, sizeof(buffer), buffer_before,
                &buffer_n, &polling_timeout, FALSE)) != TIMEOUT) {
        if (rc == ERROR) {
            /* Not sure what to do here... */
            log_error("Got ERROR while reading from %s, bailing out!",
                serial_port.filename);
            return EXIT_SERIAL_TIMEOUT;
        }
        buffer_n = 0;
        buffer_before = buffer_n;
    }

    /* All OK */
    return EXIT_OK;

}

/*
 * Write data to the serial port from the buffer.
 *
 * @param  buffer       The buffer containing bytes to write to the serial port
 * @param  buffer_n     The number of bytes in the buffer
 * @return rc           The number of bytes written, or -1 and errno set appropriately
 */
static int write_serial_port(unsigned char * buffer, int buffer_n) {
    char line[MAX_SERIAL_WRITE * 5];
    int i;
    int rc;

    assert(buffer_n <= MAX_SERIAL_WRITE);

    /* Write the data */
    rc = write(serial_port.fd, buffer, buffer_n);
    if (rc < 0) {
        /* Error */
        log_error("Error in write() call on %s: %s (%d)", serial_port.filename, strerror(errno), errno);
        return rc;
    }

    if ((DEBUG_SERIAL_IO == TRUE) && (global_options.verbose == TRUE)) {

        memset(line, 0, sizeof(line));
        for (i=0; i < rc; i++) {
            if ((buffer[i] >= 0x20) && (buffer[i] <= 0x7E)) {
                /* Printable character, special case '\' */
                if (buffer[i] == '\\') {
                    snprintf(line + strlen(line), sizeof(line) - strlen(line),
                        "\\\\");
                } else if (buffer[i] == '%') {
                    snprintf(line + strlen(line), sizeof(line) - strlen(line),
                        "\\045");
                } else {
                    snprintf(line + strlen(line), sizeof(line) - strlen(line),
                        "%c", buffer[i]);
                }
            } else {
                snprintf(line + strlen(line), sizeof(line) - strlen(line),
                    "\\%03o", buffer[i]);
            }
        }
        log_serial_write("%04d %s", buffer_n, line);

    } /* if (DEBUG_SERIAL_IO == TRUE) */

    /* All is well */
    return rc;
}

#endif /* 0 */

/*
 * Open the serial port
 *
 * @param  flags        See the man pages for open(2)
 * @return success      TRUE means OK
 *                      FALSE means port could not be opened
 */
static BOOL open_serial_port(int flags) {
    char flags_string[1024];
    memset(flags_string, 0, sizeof(flags_string));

    if ((flags & O_RDONLY) != 0) {
        sprintf(flags_string + strlen(flags_string), "READ-ONLY ");
    }
    if ((flags & O_WRONLY) != 0) {
        sprintf(flags_string + strlen(flags_string), "WRITE-ONLY ");
    }
    if ((flags & O_RDWR) != 0) {
        sprintf(flags_string + strlen(flags_string), "READ+WRITE ");
    }
    if ((flags & O_SYNC) != 0) {
        sprintf(flags_string + strlen(flags_string), "O_SYNC ");
    }
    if ((flags & O_NOCTTY) != 0) {
        sprintf(flags_string + strlen(flags_string), "O_NOCTTY ");
    }
    if ((flags & O_NONBLOCK) != 0) {
        sprintf(flags_string + strlen(flags_string), "O_NONBLOCK ");
    }

    log_info("Opening %s with the following flags: %s",
        global_options.port, flags_string);

    serial_port.fd = open(global_options.port, flags);
    if (serial_port.fd < 0) {
        log_error("Error opening %s: %s (%d)", global_options.port,
            strerror(errno), errno);
        return FALSE;
    }

    /* We don't officially need this, but just to be more complete... */
    serial_port.filename = global_options.port;

    /* Flush the input buffer */
    if (tcflush(serial_port.fd, TCIOFLUSH) < 0) {
        log_warn("Error flushing %s: %s (%d)", serial_port.filename,
            strerror(errno), errno);
    }

    /* OK */
    return TRUE;
}

/*
 * Close a serial port
 *
 * @param  none
 * @return success      TRUE means OK
 *                      FALSE means port could not be closed
 */
static BOOL close_serial_port() {

    log_info("Closing %s...", global_options.port);

    if (close(serial_port.fd) < 0) {
        log_error("Error closing %s: %s (%d)", global_options.port,
            strerror(errno), errno);
        return FALSE;
    }

    reset_port(serial_port);

    /* OK */
    return TRUE;
}

/*
 * Flush the port
 *
 * @param  none
 * @return void
 */
static int flush_port() {
    char line[MAX_SERIAL_WRITE * 5];
    unsigned char buffer[MAX_SERIAL_WRITE];
    int buffer_n;
    int buffer_before;
    int rc;
    int i;

    /* How long we will allow each poll/select to wait before seeing data */
    struct timeval polling_timeout;

    /* Get access to port */
    if (open_serial_port(O_RDWR | O_NOCTTY | O_SYNC | O_NONBLOCK) == FALSE) {
        return EXIT_SERIAL_PORT;
    }

    /* Set poll/select timeout */
    float timeout = 2.0;
    polling_timeout.tv_sec = timeout;
    polling_timeout.tv_usec = (unsigned long)(timeout * 1000000.0f) % 1000000;

    log_info("Flushing data from %s...", serial_port.filename);

    buffer_n = 0;
    buffer_before = buffer_n;
    while ((rc = read_serial_port(buffer, sizeof(buffer), buffer_before,
                &buffer_n, &polling_timeout, FALSE)) != TIMEOUT) {
        if (rc == ERROR) {
            /* Not sure what to do here... */
            log_error("Got ERROR while reading from %s, bailing out!",
                serial_port.filename);
            break;
        }

        /* For debugging, emit the results */
        memset(line, 0, sizeof(line));
        for (i=0; i < buffer_n; i++) {
            if ((buffer[i] >= 0x20) && (buffer[i] <= 0x7E)) {
                /* Printable character, special case '\' and '%' */
                if (buffer[i] == '\\') {
                    snprintf(line + strlen(line), sizeof(line) - strlen(line),
                        "\\\\");
                } else if (buffer[i] == '%') {
                    snprintf(line + strlen(line), sizeof(line) - strlen(line),
                        "\\045");
                } else {
                    snprintf(line + strlen(line), sizeof(line) - strlen(line),
                        "%c", buffer[i]);
                }
            } else {
                snprintf(line + strlen(line), sizeof(line) - strlen(line),
                    "\\%03o", buffer[i]);
            }
        }
        log_serial_read("%04d %s", buffer_n, line);

        buffer_n = 0;
        buffer_before = buffer_n;
    }

    /* Release the serial port */
    close_serial_port();

    /* All OK */
    return EXIT_OK;
}



/*
 * Command-line entry point.
 *
 * @param  argc         See parameters at the top of
 * @param  argv         ...this file
 * @return int          See return codes at top of file
 */
int main(int argc, char **argv) {
    int rc;

    /* Check arguments */
    if (default_options(argc, argv) != TRUE) {
        /* Bad arguments */
        exit(EXIT_BAD_ARGS);
    }

    /* Startup message */
    emit_startup(MY_NAME, MAKE_DATE_STRING(__DATE__, __TIME__));

    /* Do the work */
    rc = flush_port();

    /* Shutdown message */
    emit_shutdown(MY_NAME);

    if (rc != EXIT_OK) {
        /* Error */
        log_error("Exiting with RC=%d", rc);
        exit(rc);
    }

    /* Exit OK */
    exit(EXIT_OK);
}
