/*
 * Written 2022 by Autumn Lamonte ⚧ Trans Liberation Now
 *
 * This is free and unencumbered software released into the public domain.
 *
 * Anyone is free to copy, modify, publish, use, compile, sell, or distribute
 * this software, either in source code form or as a compiled binary, for any
 * purpose, commercial or non-commercial, and by any means.
 *
 * In jurisdictions that recognize copyright laws, the author or authors of
 * this software dedicate any and all copyright interest in the software to
 * the public domain. We make this dedication for the benefit of the public
 * at large and to the detriment of our heirs and successors. We intend this
 * dedication to be an overt act of relinquishment in perpetuity of all
 * present and future rights to this software under copyright law.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
 * THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
 * IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
 * CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *
 * For more information, please refer to <http://unlicense.org/>
 *
 * ----------------------------------------------------------------------------
 *
 * Timer test program.
 *
 * This program tests the system timer.
 *
 * Invocation:          timer { -z hertz } [ -v ] | -h | -? }
 *
 * Return codes:        0  OK
 *                      99 Unknown arguments
 *
 */

#include <stdlib.h>
#include <errno.h>
#include <signal.h>
#include <string.h>
#include <unistd.h>
#include "logging.h"
#include "utils.h"

#define MY_NAME         "TIMER"

/* How fast to emit the message "I am awake" */
static int hertz = 4;

/* The original SIGALRM handler */
static void (*old_sigalrm)(int);

/*
 * SIGALRM signal handler.  It needs to exist, but
 * doesn't need to do anything.
 */
static void sigalrm_handler(int s) {}

/*
 * Main data processing loop
 *
 * @param  none
 * @return void
 */
static int main_loop() {
    time_t now;
    time_t next;

    struct itimerval timer;
    long usec = 1000000 / hertz;
    log_info("usec: %d", usec);

    timer.it_value.tv_sec = 0;
    timer.it_value.tv_usec = usec;
    timer.it_interval.tv_sec = 0;
    timer.it_interval.tv_usec = usec;

    /* Get precisely onto the second boundary */
    log_info("Calibrating to the next second...");
    time(&now);
    for (;;) {
        time(&next);
        if (next != now) {
            break;
        }
    }

    if (setitimer(ITIMER_REAL, &timer, NULL) < 0) {
        log_error("Unable to set high-resolution timer: %s (%d)",
            strerror(errno), errno);
        return EXIT_OS;
    }

    /* Now enter the main loop */
    for (;;) {
        struct timespec fulltime;
        sleep(1);

        clock_gettime(CLOCK_REALTIME, &fulltime);
        log_info("%d I am alive", fulltime.tv_nsec / 1000);
    } /* for (;;) */

    /*
     * NOTE:  This point is only reached when we've been SIGTERM'd.
     *
     */

    /* All OK */
    return EXIT_OK;
}

/*
 * Show the usage options:
 *      -h -?            Usage screen
 *      -z hertz         How fast to emit message
 *      -v               Verbose output
 *
 * @param  progname     Program name
 * @return void
 */
static void show_usage(const char * progname) {
    fprintf(stderr, "USAGE: %s { -h | -? | { -p /dev/ttyXX } [ -v ] }\n",
        progname);
    fprintf(stderr, "       -h -?            Display usage screen\n");
    fprintf(stderr, "       -z hertz         How fast to emit message\n");
    fprintf(stderr, "       -v               Verbose output\n");
}

/*
 * Process the command-line options:
 *      -h -?            Usage screen
 *      -z hertz         How fast to emit message
 *      -v               Verbose output
 *
 * @param  argc         main() argc
 * @param  argv         main() argv
 * @return void
 */
static BOOL process_options(int argc, char ** argv) {
    char cmdline_options[] = "h?z:v";
    int opt;

    while ((opt = getopt(argc, argv, cmdline_options)) != -1) {

        switch (opt) {

        case '?':
        case 'h':
            show_usage(argv[0]);
            return FALSE;

        case 'z':
            hertz = atoi(optarg);
            break;

        case 'v':
            global_options.verbose = TRUE;
            break;

        default:
            return FALSE;

        } /* switch */
    } /* while */

    /* All OK */
    return TRUE;
}

/*
 * Command-line entry point.
 *
 * @param  argc         See parameters at the top of
 * @param  argv         ...this file
 * @return int          See return codes at top of file
 */
int main(int argc, char **argv) {
    time_t now;
    char * current_time;
    int rc;

    /* Check arguments */
    if (process_options(argc, argv) != TRUE) {
        /* Bad arguments */
        exit(EXIT_BAD_ARGS);
    }

    /* Startup message */
    now = time(NULL);
    current_time = ctime(&now);
    current_time[strlen(current_time)-1] = 0;
    log_info("%s: starting up, PID is %d, PPID is %d, local time is %s...",
        MY_NAME, getpid(), getppid(), current_time);
    log_info("Hertz:          %d", hertz);

    /* Compute starttime */
    starttime = time(NULL);

    /* Set the SIGALRM signal handler */
    old_sigalrm = signal(SIGALRM, &sigalrm_handler);
    if (old_sigalrm == SIG_ERR) {
        log_error("Couldn't change SIGALRM signal handler: %s",
            strerror(errno));
        exit(EXIT_OS);
    }

    /* Do the work */
    rc = main_loop();

    /* Shutdown message.  We can re-use the one from utils.c */
    emit_shutdown(MY_NAME);

    if (rc != EXIT_OK) {
        /* Error */
        log_error("Exiting with RC=%d", rc);
        exit(rc);
    }

    /* Exit OK */
    exit(EXIT_OK);
}
